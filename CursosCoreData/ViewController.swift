//
//  ViewController.swift
//  CursosCoreData
//
//  Created by Javier Flores Càrdenas on 5/1/21.
//  Copyright © 2021 Javier Flores Càrdenas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tablaCursos: UITableView!
    var cursos:[Curso] = []
    
    /*func crearCursos() -> [Curso] {
        let curso1 = Curso()
        curso1.nombre = "Programaciòn en Mòviles Avanzada"
        curso1.promedioPracticas = 20
        curso1.promedioLaboratorios = 20
        curso1.promedioFinal = 20
        let curso2 = Curso()
        curso2.nombre = "Programaciòn en Mòviles Avanzada"
        curso2.promedioPracticas = 20
        curso2.promedioLaboratorios = 20
        curso2.promedioFinal = 12
        return [curso1, curso2]
    }*/
    override func viewDidLoad() {
        super.viewDidLoad()
        tablaCursos.delegate = self
        tablaCursos.dataSource = self
        //cursos = crearCursos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        obtenerTareas()
        tablaCursos.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cursos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tablaCursos.dequeueReusableCell(withIdentifier: "Celda", for: indexPath)
        let curso = cursos[indexPath.row]
        cell.textLabel?.text = curso.nombre!
        cell.detailTextLabel?.text = "PROMEDIO FINAL:  \(String(curso.promedioFinal))"
        if curso.promedioFinal >= 13.0{
            cell.contentView.backgroundColor = hexStringToUIColor(hex: "#bcfeb3")
        }else{
            cell.contentView.backgroundColor = hexStringToUIColor(hex: "#ffa39f")
        }
        return cell
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
    
        if ((cString.count) != 6) {
            return UIColor.gray
        }
    
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
    
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    @IBAction func agregarCurso(_ sender: Any) {
        performSegue(withIdentifier: "segueAgregar", sender: nil)
    }
    
    func obtenerTareas(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            cursos = try context.fetch(Curso.fetchRequest()) as! [Curso]
        }catch{
            print("Error al leer entidad de CoreData")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueCursoDetalle"{
            let siguienteVC = segue.destination as! ViewControllerDetalle
            siguienteVC.curso = sender as! Curso
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let curso = cursos[indexPath.row]
        performSegue(withIdentifier: "segueCursoDetalle", sender: curso)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            context.delete(cursos[indexPath.row])
            cursos.remove(at: indexPath.row)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            tablaCursos.reloadData()	
        }
    }
}

