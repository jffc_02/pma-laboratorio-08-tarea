//
//  ViewControllerFormulario.swift
//  CursosCoreData
//
//  Created by Javier Flores Càrdenas on 5/1/21.
//  Copyright © 2021 Javier Flores Càrdenas. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerFormulario: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtPromPracticas: UITextField!
    @IBOutlet weak var txtPromLaboratorios: UITextField!
    
    @IBAction func botonAgregar(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Curso")
        fetchRequest.predicate = NSPredicate(format: "notificationId = 13")
        let curso = Curso(context:context)
        curso.nombre = txtNombre.text!
        curso.promedioPracticas =
            Double(txtPromPracticas.text!)!
        curso.promedioLaboratorios = Double(txtPromLaboratorios.text!)!
        curso.promedioFinal = curso.promedioLaboratorios * 0.7 + curso.promedioPracticas * 0.3
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        /*anteriorVC.cursos.append(curso)
        anteriorVC.tablaCursos.reloadData()*/
        navigationController?.popViewController(animated: true)
        
    }
}
	
