//
//  ViewControllerDetalle.swift
//  CursosCoreData
//
//  Created by Javier Flores Càrdenas on 5/1/21.
//  Copyright © 2021 Javier Flores Càrdenas. All rights reserved.
//

import UIKit
import CoreData

class ViewControllerDetalle: UIViewController {
    var curso = Curso()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNombre.text = curso.nombre
        txtPromPracticas.text = String(curso.promedioPracticas)
        txtPromFinal.text = String(curso.promedioFinal)
        txtPromLaboratorios.text = String(curso.promedioLaboratorios)
        txtPromPracticas.isUserInteractionEnabled = false
        txtNombre.isUserInteractionEnabled = false
        txtPromLaboratorios.isUserInteractionEnabled = false
    }

    
    @IBOutlet weak var btnEditar: UIButton!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtPromPracticas: UITextField!
    @IBOutlet weak var txtPromFinal: UITextField!
    @IBOutlet weak var txtPromLaboratorios: UITextField!
    @IBAction func botonEditar(_ sender: Any) {
        if btnEditar.titleLabel?.text == "Editar" {
            txtPromPracticas.isUserInteractionEnabled = true
            txtNombre.isUserInteractionEnabled = true
            txtPromLaboratorios.isUserInteractionEnabled = true
            btnEditar.isUserInteractionEnabled = true
            btnEditar.setTitle("Guardar", for: .normal)
        }else{
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            self.curso.nombre = txtNombre.text
            self.curso.promedioLaboratorios = Double(txtPromLaboratorios.text!)!
            self.curso.promedioPracticas = Double(txtPromPracticas.text!)!
            self.curso.promedioFinal = Double(txtPromLaboratorios.text!)! * 0.7 + Double(txtPromPracticas.text!)! * 0.3
            //navigationController?.popViewController(animated: true)
            do{
                try context.save()
            }catch{
                print("Error")
            }
            txtPromPracticas.isUserInteractionEnabled = false
            txtNombre.isUserInteractionEnabled = false
            txtPromLaboratorios.isUserInteractionEnabled = false
            btnEditar.isUserInteractionEnabled = false
            btnEditar.setTitle("Editar", for: .normal)
        }
    }
}
